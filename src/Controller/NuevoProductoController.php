<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\NuevoproductoType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class NuevoProductoController extends AbstractController
{
    /**
     * @Route("/nuevo/producto", name="nuevo_producto")
     */
    public function index(Request $request): Response
    {
        $Producto= new Product();
        $form=$this->createForm(NuevoproductoType::class,$Producto);
        $form->handleRequest($request);
           
            if($form->isSubmitted() && $form->isValid()){
                $em=$this->getDoctrine()->getManager();
                $em->persist($Producto);
                $em=flush();
                $this->addFlash('exito',"Se ha registrado el producto");
                return $this->redirectToRoute("nuevo_producto");
            }else{

            }
        return $this->render('nuevo_producto/index.html.twig', [
            'controller_name' => 'Nuevo Producto',
            'formulario'=>$form->createView()
        ]);
    }
}
